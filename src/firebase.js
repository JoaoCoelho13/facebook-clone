import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyDALq0GXmxsfBjSK2v0pqgy023TZQl7pYo",
    authDomain: "facebook-clone-5457a.firebaseapp.com",
    databaseURL: "https://facebook-clone-5457a.firebaseio.com",
    projectId: "facebook-clone-5457a",
    storageBucket: "facebook-clone-5457a.appspot.com",
    messagingSenderId: "631682536592",
    appId: "1:631682536592:web:b12e816bc8acc8c60ef415",
    measurementId: "G-N4H37B9ZBC"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider };
export default db;
