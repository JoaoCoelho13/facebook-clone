import React from 'react'
import "./Login.css";
import { Button } from "@material-ui/core";
import { auth, provider } from './firebase';
import { actionTypes } from './reducer';
import { useStateValue } from "./StateProvider";

function Login() {

    const [state, dispatch] = useStateValue();

    const signIn = () => {
        // sign in...
        auth
        .signInWithPopup(provider)
        .then(result => {

            dispatch({
                type: actionTypes.SET_USER,
                user: result.user,
            });

            console.log(result.user)
        })
        .catch(error => alert(error.message));
    };

    return (
        <div className="login">
            <div className="login__logo">
                <img
                    src="https://i.pinimg.com/736x/fb/7e/be/fb7ebe224986c93ab8b7ba9ecafb9877.jpg"
                    alt=""
                />
                <img
                    src="https://seeklogo.com/images/B/brownes_chill_cold_coffee-logo-BB8110C452-seeklogo.com.png"
                    alt=""
                />
            </div>
            <Button type="submit" onClick={signIn}>
                Sign in
            </Button>
        </div>
    );
}

export default Login
